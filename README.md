# Tetris #

WebGL + JS implementation of popular game.

### Environment ###

Windows/Linux + [NodeJS LTS](https://nodejs.org)

### How do I get set up? ###

* Clone this repository
* Go to the project folder
* Type in cmd/terminal

`npm install`

`npm run dev`

Browser will automatically open [URL with the game](http://localhost:3000/)

### Who do I talk to? ###

* Alexey Goncharenko [gon4arenko.alexey@gmail.com](mailto:gon4arenko.alexey@gmail.com)
* Elena Lebedeva [wind.is.my.brother@gmail.com](mailto:wind.is.my.brother@gmail.com)