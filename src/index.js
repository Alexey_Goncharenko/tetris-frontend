import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, browserHistory, Route, Redirect} from 'react-router-dom';
import Sound from 'react-sound';

import Tetris from './components/tetris';
import NavbarComponent from './components/navbar';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      music: true,
      sounds: true
    };
  }

  render() {
    return (
      <div>
        <NavbarComponent
          onSwitchMusic={() => this.setState({music: !this.state.music})}
          onSwitchSounds={() => this.setState({sounds: !this.state.sounds})}
          {...this.state}
        />
        <Route
          exact path='/'
          render={() => (
            <Redirect to='/game-webgl' />
          )}
        />

        <Route
          path='/game'
        >
          <Tetris sounds={this.state.sounds} />
        </Route>

        <Sound
          url="/dist/assets/music.mp3"
          playStatus={this.state.music ? Sound.status.PLAYING : Sound.status.STOPPED}
          playFromPosition={0}
          loop
          volume={50}
        />
      </div>
    );
  }
}

const mountNode = document.getElementById('root');

ReactDOM.render(
  <Router history={browserHistory}>
    <App />
  </Router>,
  mountNode
);
