import React from 'react';
import PropTypes from 'prop-types';
import {initShaderProgram, vsSource, fsSource, initBuffers, drawScene} from './graphics/index';

class FieldComponent extends React.Component {
  constructor(props) {
    super(props);

    this.fieldSize = {
      nx: 10,
      ny: 20
    };

    this.state = {
      width: 200,
      height: 400,
      field: []
    };

    this.updateField = this.updateField.bind(this);
  }

  componentDidMount() {
    const {canvas} = this;
    // Initialize the GL context
    const gl = canvas.getContext('webgl');

    // Only continue if WebGL is available and working
    if (!gl) {
      // eslint-disable-next-line no-alert
      alert('Unable to initialize WebGL. Your browser or machine may not support it.');
      return;
    }

    // Set clear color to black, fully opaque
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    // Clear the color buffer with specified clear color
    gl.clear(gl.COLOR_BUFFER_BIT);

    const shaderProgram = initShaderProgram(gl, vsSource, fsSource);

    this.programInfo = {
      program: shaderProgram,
      attribLocations: {
        vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
        texCoords: gl.getAttribLocation(shaderProgram, 'aTexPosition'),
        colorPosition: gl.getAttribLocation(shaderProgram, 'aVertexColor')
      },
      uniformLocations: {
        projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
        modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
        texture: gl.getUniformLocation(shaderProgram, 'Tex')
      }
    };

    const buffers = initBuffers(gl, this.fieldSize.nx, this.fieldSize.ny);
    drawScene(gl, this.programInfo, buffers, this.fieldSize.nx, this.fieldSize.ny, this.state.field);
  }

  componentWillReceiveProps({field}) {
    this.setState({field}, this.updateField);
  }

  updateField() {
    const gl = this.canvas.getContext('webgl');

    const buffers = initBuffers(gl, this.fieldSize.nx, this.fieldSize.ny, this.state.field);
    drawScene(gl, this.programInfo, buffers, this.fieldSize.nx, this.fieldSize.ny, this.state.field);
  }

  render() {
    return (
      <canvas ref={canvas => { this.canvas = canvas; }} width={this.state.width} height={this.state.height} />
    );
  }
}

FieldComponent.propTypes = {
  field: PropTypes.array
};

FieldComponent.defaultProps = {
  field: []
};

export default FieldComponent;
