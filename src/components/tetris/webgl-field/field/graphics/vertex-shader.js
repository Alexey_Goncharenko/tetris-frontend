export default `
    precision highp float;
    attribute vec4 aVertexPosition;
    attribute vec2 aTexPosition;
    attribute vec3 aVertexColor;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;
    
    varying vec3 Color;
    varying vec2 TexCoord;

    uniform sampler2D Tex;
    
    void main() {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      TexCoord = aTexPosition;
      Color = aVertexColor;
    }
`;
