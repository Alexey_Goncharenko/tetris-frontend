export default `
    precision highp float;
    varying vec3 Color;
    varying vec2 TexCoord;
    
    uniform sampler2D Tex;
    
    void main() {
      vec4 texColor = texture2D(Tex, TexCoord);
      gl_FragColor = vec4(Color, 1.0) + texColor;
    }
`;
