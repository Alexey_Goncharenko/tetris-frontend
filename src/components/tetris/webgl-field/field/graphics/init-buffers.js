export default function (gl, nx, ny, field) {

  // Create a buffer for the square's positions.

  const positionBuffer = gl.createBuffer();

  // Select the positionBuffer as the one to apply buffer
  // operations to from here out.

  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  // Now create an array of positions for the square.

  const h = 1;

  let positions = new Array(nx * ny * 4 * 2);
  for (let i = 0; i < nx; i++)
  {
    for (let j = 0; j < ny; j++)
    {
      positions[8 * (ny * i + j)] = h * i;
      positions[8 * (ny * i + j) + 1] = h * j;
      positions[8 * (ny * i + j) + 2] = h * (i + 1);
      positions[8 * (ny * i + j) + 3] = h * j;
      positions[8 * (ny * i + j) + 4] = h * i;
      positions[8 * (ny * i + j) + 5] = h * (j + 1);
      positions[8 * (ny * i + j) + 6] = h * (i + 1);
      positions[8 * (ny * i + j) + 7] = h * (j + 1);
    }
  }

  gl.bufferData(
    gl.ARRAY_BUFFER,
    new Float32Array(positions),
    gl.STATIC_DRAW
  );

  const colorBuffer = gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);

  let colors = new Array(nx * ny * 4 * 3);
  for (let i = 0; i < nx; i++) {
    for (let j = 0; j < ny; j++) {
      for (let k = 0; k < 4; k++) {
        colors[3 * (4 * ny * i + j * 4 + k)] = field && field[j] && field[j][i] ? field[j][i][0] : 0;
        colors[3 * (4 * ny * i + j * 4 + k) + 1] = field && field[j] && field[j][i] ? field[j][i][1] : 0;
        colors[3 * (4 * ny * i + j * 4 + k) + 2] = field && field[j] && field[j][i] ? field[j][i][2] : 0;
      }
    }
  }

  // Now pass the list of positions into WebGL to build the
  // shape. We do this by creating a Float32Array from the
  // JavaScript array, then use it to fill the current buffer.

  gl.bufferData(
    gl.ARRAY_BUFFER,
    new Float32Array(colors),
    gl.STATIC_DRAW
  );

  const textureBuffer = gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, textureBuffer);

  let texCoords = new Array(nx * ny * 4 * 2);
  for (let i = 0; i < nx; i++) {
    for (let j = 0; j < ny; j++)
    {
      texCoords[8 * (ny * i + j)] = 0;
      texCoords[8 * (ny * i + j) + 1] = 1;
      texCoords[8 * (ny * i + j) + 2] = 1;
      texCoords[8 * (ny * i + j) + 3] = 1;
      texCoords[8 * (ny * i + j) + 4] = 0;
      texCoords[8 * (ny * i + j) + 5] = 0;
      texCoords[8 * (ny * i + j) + 6] = 1;
      texCoords[8 * (ny * i + j) + 7] = 0;
    } }

  // Now pass the list of positions into WebGL to build the
  // shape. We do this by creating a Float32Array from the
  // JavaScript array, then use it to fill the current buffer.

  gl.bufferData(
    gl.ARRAY_BUFFER,
    new Float32Array(texCoords),
    gl.STATIC_DRAW
  );

  return {
    position: positionBuffer,
    color: colorBuffer,
    texCoord: textureBuffer
  };
}
