import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './game.scss';
import FieldComponent from './field';

class HTMLField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      field: []
    };
  }

  componentWillReceiveProps({field, figure, nextFigure}) {
    if (!field.data) {
      this.setState({field: []});
      return;
    }
    let newField = [];
    for (let row of field.data.slice().reverse()) {
      newField.push([...row]);
    }

    for (let i = 0; i < figure.data.length; i++) {
      for (let j = 0; j < figure.data[0].length; j++) {
        if (figure.data[i][j] === 1) {
          newField[newField.length - figure.corner.bottom - i - 1][figure.corner.left + j] = {
            color: figure.color,
            colorRGB: figure.colorRGB
          };
        }
      }
    }

    const newFigure = nextFigure.data &&
      nextFigure.data.map(row => row.map(i => i === 0 ? 0 : {color: nextFigure.color})).reverse();

    for (let i = 0; i < 4; i++) {
      if (!newFigure[i]) {
        newFigure.push(new Array(3).fill(0));
      }
      for (let j = newFigure[i].length; j < 3; j++) {
        newFigure[i].push(0);
      }
    }

    newField = newField.slice(4).map(item => item.map(i => i.colorRGB));

    this.setState({field: newField.reverse(), nextFigure: newFigure});
  }

  render() {
    return (
      <div>
        {this.state.field.length > 0 && (<div className='field-container'>
          <FieldComponent
            field={this.state.field}
            nextFigure={this.state.nextFigure}
          />
        </div>
        )}

        <div className='next-figure-container'>
          NEXT FIGURE
          {this.state.nextFigure && this.state.nextFigure.map((row, i) => (
            <div key={i} className='blocks-row'>
              {row && row.map((block, j) => (
                <div key={j} className='block block--bg-gray' style={{background: block.color}} />
              ))}
            </div>
          ))}
        </div>
        <div className='level-score-container'>
          LEVEL
          <div className='level'>
            <div className='level__bg' style={{height: this.props.nextLevelPercent + '%'}}/>
            {this.props.level}
          </div>

          SCORES
          <div className='scores'>{this.props.scores}</div>
        </div>
      </div>
    );
  }

}

HTMLField.propTypes = {
  field: PropTypes.object.isRequired,
  figure: PropTypes.object,
  nextFigure: PropTypes.object,
  level: PropTypes.number,
  scores: PropTypes.number,
  nextLevelPercent: PropTypes.number
};

HTMLField.defaultProps = {};

export default HTMLField;
