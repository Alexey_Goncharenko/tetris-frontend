import React, {Component} from 'react';
import PropTypes from 'prop-types';
import keydown from 'react-keydown';
import Sound from 'react-sound';

import directions from '../../controllers/figure/directions';
import {Figure as FigureController, Field as FieldController} from '../../controllers';
import HTMLField from './html-field';
import WebGLField from './webgl-field';
import {Route} from 'react-router-dom';

class Tetris extends Component {
  constructor(props) {
    super(props);

    this.state = {
      field: {},
      currentFigure: {},
      nextFigure: {},
      width: 10,
      height: 24,
      level: 0,
      scores: 0,
      nextLevelPercent: 0,
      gameStarted: false,
      levelChanged: false,
      playBing: false,
      playSlide: false
    };

    this.defaultCorner = {left: this.state.width / 2 - 1, bottom: this.state.height - 4};
    this.limits = [0, 100, 250, 500, 1000, 5000, 10000, 100000];
    this.periods = [600, 400, 300, 250, 200, 150, 100, 75];

    this.startGame = this.startGame.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.turnRight = this.turnRight.bind(this);
    this.pushFigure = this.pushFigure.bind(this);
    this.manageLevel = this.manageLevel.bind(this);
    this.updateGame = this.updateGame.bind(this);
  }

  @keydown('down', 'right', 'left')
  handleKeyDown(event) {
    if (!this.state.gameStarted) {
      return;
    }
    let direction = directions.none;

    switch (event.key) {
    case 'ArrowLeft':
      direction = directions.left;
      break;
    case 'ArrowRight':
      direction = directions.right;
      break;
    case 'ArrowDown':
      direction = directions.down;
      break;
    default:
      break;
    }

    const newFigure = this.state.currentFigure.tryStep(direction);
    if (!this.state.field.hasCollision(newFigure)) {
      this.setState({currentFigure: newFigure});
    }
  }

  @keydown('up')
  turnRight() {
    if (!this.state.gameStarted) {
      return;
    }
    const newFigure = this.state.currentFigure.tryTurnRight();
    if (!this.state.field.hasCollision(newFigure)) {
      this.setState({currentFigure: newFigure});
    }
  }

  @keydown('space')
  pushFigure() {
    if (!this.state.gameStarted) {
      return;
    }
    this.state.field.pushFigure(this.state.currentFigure);
    const rowsReleased = this.state.field.releaseFullRows();
    this.setState({
      currentFigure: this.state.nextFigure,
      nextFigure: new FigureController({corner: this.defaultCorner}),
      field: {...this.state.field},
      scores: this.state.scores + 5 + 50 * rowsReleased,
      playSlide: rowsReleased !== 0,
      playBing: rowsReleased === 0
    }, this.manageLevel);
  }

  manageLevel() {
    if (this.state.field.isGameOver()) {
      clearInterval(this.intervalID);
      this.setState({gameStarted: false});
    }

    for (let i = 0; i < this.limits.length - 1; i++) {
      if (this.state.scores >= this.limits[i] && this.state.scores < this.limits[i + 1]) {
        this.setState({
          levelChanged: this.state.levelChanged || (this.state.level !== i),
          level: i,
          nextLevelPercent: 100 * (this.state.scores - this.limits[i]) / (this.limits[i + 1] - this.limits[i])
        });
        return;
      }
    }

    this.setState({
      levelChanged: this.state.levelChanged || (this.state.level !== this.limits.length - 1),
      level: this.limits.length - 1,
      nextLevelPercent: 100
    });
  }

  updateGame() {
    const newFigure = this.state.currentFigure.tryStep(directions.down);
    if (this.state.field.hasCollision(newFigure)) {
      this.state.field.addFigure(this.state.currentFigure);
      const rowsReleased = this.state.field.releaseFullRows();
      this.setState({
        field: {...this.state.field},
        currentFigure: {...this.state.nextFigure},
        nextFigure: new FigureController({corner: this.defaultCorner}),
        scores: this.state.scores + 5 + 50 * rowsReleased,
        playSlide: rowsReleased !== 0,
        playBing: rowsReleased === 0
      }, this.manageLevel);

      if (this.state.field.isGameOver()) {
        clearInterval(this.intervalID);
        this.setState({gameStarted: false});
      }
    } else {
      this.setState({currentFigure: newFigure});
    }

    if (this.state.levelChanged) {
      clearInterval(this.intervalID);
      this.intervalID = setInterval(this.updateGame, this.periods[this.state.level]);
      this.setState({levelChanged: false});
    }
  }

  startGame(level) {
    const field = new FieldController(this.state);
    const currentFigure = new FigureController({corner: this.defaultCorner});
    const nextFigure = new FigureController({corner: this.defaultCorner});

    this.setState({field, nextFigure, currentFigure, level: 0, scores: 0, gameStarted: true, nextLevelPercent: 0});
    clearInterval(this.intervalID);
    this.intervalID = setInterval(this.updateGame, this.periods[level]);
  }

  render() {
    return (
      <div className='main-container'>
        {!this.state.gameStarted && (
          <button className='start-button btn btn-primary' onClick={() => this.startGame(0)}>START GAME</button>
        )}
        <Route path='/game-html' render={() => (
          <HTMLField
            field={this.state.gameStarted ? this.state.field : {}}
            figure={this.state.currentFigure}
            nextFigure={this.state.nextFigure}
            level={this.state.level}
            scores={this.state.scores}
            nextLevelPercent={this.state.nextLevelPercent}
          />
        )}
        />
        <Route path='/game-webgl' render={() => (
          <WebGLField
            field={this.state.gameStarted ? this.state.field : {}}
            figure={this.state.currentFigure}
            nextFigure={this.state.nextFigure}
            level={this.state.level}
            scores={this.state.scores}
            nextLevelPercent={this.state.nextLevelPercent}
          />
        )}
        />

        {this.props.sounds && this.state.playBing && (
          <Sound
            url="/dist/assets/bing.mp3"
            playStatus={Sound.status.PLAYING}
            playFromPosition={0}
            onFinishedPlaying={() => this.setState({playBing: false})}
          />
        )}
        {this.props.sounds && this.state.playSlide && (
          <Sound
            url="/dist/assets/slide.mp3"
            playStatus={Sound.status.PLAYING}
            playFromPosition={0}
            onFinishedPlaying={() => this.setState({playSlide: false})}
          />
        )}
      </div>
    );
  }
}

Tetris.propTypes = {
  sounds: PropTypes.bool
};

Tetris.defaultProps = {
  sounds: true
};

export default Tetris;
