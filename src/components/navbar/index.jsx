import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Nav, Navbar, NavItem} from 'react-bootstrap';
import Switch from 'react-switch';

class NavbarComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (<Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="/">Tetris</a>
        </Navbar.Brand>
      </Navbar.Header>
      <Nav>
        <NavItem eventKey={1} href="/game-webgl">WebGL version</NavItem>
        <NavItem eventKey={2} href="/game-html">HTML version</NavItem>
      </Nav>
      <Nav pullRight>
        <NavItem>
          <div className='switch-container'>
            <span className='switcher-description'> Music</span>
            <span className='switcher-element'>
              <Switch checked={this.props.music} onChange={this.props.onSwitchMusic}/>
            </span>
          </div>
        </NavItem>
        <NavItem>
          <div className='switch-container switcher-container-sound'>
            <span className='switcher-description'>Sounds</span>
            <span className='switcher-element'>
              <Switch checked={this.props.sounds} onChange={this.props.onSwitchSounds}/>
            </span>
          </div>
        </NavItem>
      </Nav>
    </Navbar>);
  }
}

NavbarComponent.propTypes = {
  music: PropTypes.bool,
  sounds: PropTypes.bool,
  onSwitchMusic: PropTypes.func,
  onSwitchSounds: PropTypes.func
};

NavbarComponent.defaultProps = {
  music: true,
  sounds: true
};

export default NavbarComponent;
