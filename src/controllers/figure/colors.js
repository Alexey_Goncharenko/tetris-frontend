const colors = [
  [137, 0, 22],
  [28, 190, 45],
  [0, 123, 196],
  [255, 170, 0],
  [170, 0, 201],
  [0, 185, 173],
  [151, 83, 18]
];

export const colorsRGB = colors.map(item => item.map(i => i / 255));

export const colorsHEX = [
  '#890016',
  '#1CBE2D',
  '#007BC4',
  '#FFAA00',
  '#AA00C9',
  '#00B9AD',
  '#975312'
];
