import deepcopy from 'deepcopy';

import types from './types';
import {colorsHEX, colorsRGB} from './colors';

class Figure {
  constructor({corner, type, color, colorRGB, data}) {
    this.corner = {...corner};
    this.type = type || Math.random() * types.length | 0;
    this.color = color || colorsHEX[this.type];
    this.colorRGB = colorRGB || colorsRGB[this.type];
    this.data = data || deepcopy(types[this.type]);

    this.tryStep = this.tryStep.bind(this);
    this.tryTurnRight = this.tryTurnRight.bind(this);
  }

  tryTurnRight() {
    const newData = [];
    const height = this.data[0].length, width = this.data.length;

    for (let i = 0; i < height; i++) {
      newData.push(new Array(width));
    }

    for (let i = 0; i < height; i++) {
      for (let j = 0; j < width; j++) {
        newData[height - i - 1][width - j - 1] = this.data[width - j - 1][i];
      }
    }

    return new Figure({
      ...this,
      data: newData
    });
  }

  tryStep(direction) {
    const newCorner = {
      bottom: this.corner.bottom + direction[0],
      left: this.corner.left + direction[1]
    };

    return new Figure({
      ...this,
      corner: newCorner
    });
  }
}

export default Figure;
