export default {
  down: [-1, 0],
  left: [0, -1],
  right: [0, 1],
  none: [0, 0]
};
