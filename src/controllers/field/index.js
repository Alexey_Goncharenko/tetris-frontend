import directions from '../figure/directions';

class Field {
  constructor({width = 10, height = 24}) {
    this.data = [];
    this.width = width;
    this.height = height;

    for (let i = 0; i < this.height; i++) {
      this.data.push(new Array(this.width).fill(0));
    }

    this.hasCollision = this.hasCollision.bind(this);
    this.addFigure = this.addFigure.bind(this);
    this.releaseFullRows = this.releaseFullRows.bind(this);
    this.pushFigure = this.pushFigure.bind(this);
    this.isGameOver = this.isGameOver.bind(this);
  }

  hasCollision(figure) {
    const x = figure.corner.left, y = figure.corner.bottom;

    if (x < 0 || y < 0 || x + figure.data[0].length > this.width) {
      return true;
    }

    for (let i = 0; i < figure.data.length; i++) {
      for (let j = 0; j < figure.data[0].length; j++) {
        if (figure.data[i][j] && this.data[y + i][x + j]) {
          return true;
        }
      }
    }

    return false;
  }

  releaseFullRows() {
    let rowsRemoved = 0;
    for (let i = this.data.length - 1; i >= 0; i--) {
      const row = this.data[i];
      if (row.every(c => c !== 0)) {
        this.data.splice(i, 1);
        rowsRemoved++;
      }
    }

    for (let i = 0; i < rowsRemoved; i++) {
      this.data.push(new Array(this.width).fill(0));
    }

    return rowsRemoved;
  }

  pushFigure(figure) {
    let newFigure = figure.tryStep(directions.down), oldFigure = figure;
    while (!this.hasCollision(newFigure)) {
      oldFigure = newFigure;
      newFigure = newFigure.tryStep(directions.down);
    }
    this.addFigure(oldFigure);
  }

  isGameOver() {
    return !this.data[this.height - 4].every(c => c === 0);
  }

  addFigure(figure) {
    const x = figure.corner.left, y = figure.corner.bottom;

    for (let i = 0; i < figure.data.length; i++) {
      for (let j = 0; j < figure.data[0].length; j++) {
        if (figure.data[i][j]) {
          this.data[y + i][x + j] = {color: figure.color, colorRGB: figure.colorRGB};
        }
      }
    }
  }
}

export default Field;
